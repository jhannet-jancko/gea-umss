package org.umss.geaumss.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.umss.geaumss.models.Faculty;
import org.umss.geaumss.models.University;

import java.util.List;

@Component
public interface FacultyRepository extends  JpaRepository<Faculty, Integer> {

    @Query("SELECT f FROM Faculty f WHERE f.uuid = ?1")
    Faculty findOneByUuid(String uuid);
/*
    @Query("SELECT f FROM Faculty f WHERE f.university = ?1")
    List<Faculty> findAllByUniversityId(University university);*/

}

