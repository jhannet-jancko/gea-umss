package org.umss.geaumss.dtos;

import org.springframework.stereotype.Component;
import org.umss.geaumss.models.University;

@Component
public class UniversityMapper {
    public UniversityDTO toDTO(University university) {
        UniversityDTO universityDTO = new UniversityDTO();
        universityDTO.setUuid(university.getUuid());
        universityDTO.setCode(university.getCode());
        universityDTO.setName(university.getName());
        universityDTO.setCreatedDate(university.getCreatedDate());
        universityDTO.setModifiedDate(university.getModifiedDate());

        return universityDTO;
    }

    public University toUniversity(UniversityDTO universityDTO) {
        University university = new University();
        university.setUuid(universityDTO.getUuid());
        university.setCode(universityDTO.getCode());
        university.setName(universityDTO.getName());
        university.setCreatedDate(universityDTO.getCreatedDate());
        university.setModifiedDate(universityDTO.getModifiedDate());

        return university;
    }
}
