package org.umss.geaumss.dtos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.umss.geaumss.models.Faculty;
import org.umss.geaumss.models.University;

@Component
public class FacultyMapper {

    @Autowired
    private UniversityMapper universityMapper;

    public FacultyDTO toDTO(Faculty faculty, boolean mapUniversity) {
        FacultyDTO facultyDTO = new FacultyDTO();
        facultyDTO.setUuid(faculty.getUuid());
        facultyDTO.setCode(faculty.getCode());
        facultyDTO.setName(faculty.getName());
        facultyDTO.setDeanName(faculty.getDeanName());
        facultyDTO.setAddress(faculty.getAddress());
        facultyDTO.setMission(faculty.getMission());
        facultyDTO.setVision(faculty.getVision());
        facultyDTO.setTelephone(faculty.getTelephone());
        facultyDTO.setEmail(faculty.getEmail());
        facultyDTO.setWebSite(faculty.getWebSite());
        facultyDTO.setCreatedDate(faculty.getCreatedDate());
        facultyDTO.setModifiedDate(faculty.getModifiedDate());
        if (mapUniversity) {
            facultyDTO.setUniversity(universityMapper.toDTO(faculty.getUniversity()));
        }
        return facultyDTO;
    }

    public Faculty getFaculty(FacultyDTO facultyDTO, University university) {
        Faculty faculty = new Faculty();
        faculty.setUuid(facultyDTO.getUuid());
        faculty.setCode(facultyDTO.getCode());
        faculty.setName(facultyDTO.getName());
        faculty.setDeanName(facultyDTO.getDeanName());
        faculty.setAddress(facultyDTO.getAddress());
        faculty.setMission(facultyDTO.getMission());
        faculty.setVision(facultyDTO.getVision());
        faculty.setTelephone(facultyDTO.getTelephone());
        faculty.setEmail(facultyDTO.getEmail());
        faculty.setWebSite(facultyDTO.getWebSite());
        faculty.setCreatedDate(facultyDTO.getCreatedDate());
        faculty.setModifiedDate(facultyDTO.getModifiedDate());
        faculty.setUniversity(university);
        return faculty;
    }
}