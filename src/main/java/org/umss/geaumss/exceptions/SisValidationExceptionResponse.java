package org.umss.geaumss.exceptions;

import java.util.Date;
import java.util.Map;

public class SisValidationExceptionResponse extends SisExceptionResponse {

    private Map<String, String> fieldErrors;

    SisValidationExceptionResponse(Date timestamp, String message, String path, Map<String, String> fieldErrors) {
        super(timestamp, message, path);
        this.fieldErrors = fieldErrors;
    }

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }
}
