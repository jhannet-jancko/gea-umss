package org.umss.geaumss.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.umss.geaumss.dtos.FacultyDTO;
import org.umss.geaumss.dtos.UniversityDTO;
import org.umss.geaumss.services.UniversityService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "/universities")
public class UniversityController {

    @Autowired
    UniversityService universityService;

    @GetMapping
    public ResponseEntity<List<UniversityDTO>> getAll() {
        return ResponseEntity.ok().body(universityService.getAllUniversities());
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<UniversityDTO> get(@PathVariable String uuid) {
        return ResponseEntity.ok().body(universityService.getUniversity(uuid));
    }

    @PostMapping
    public ResponseEntity<UniversityDTO> create(@Valid @RequestBody UniversityDTO universityDTO) {
        UniversityDTO createdUniversityDTO = universityService.save(universityDTO);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{universityUuid}")
                .buildAndExpand(createdUniversityDTO.getUuid())
                .toUri();

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(createdUniversityDTO);
    }

    @PutMapping("/{uuid}")
    public ResponseEntity<UniversityDTO> update(@PathVariable String uuid, @RequestBody UniversityDTO universityDTO) {
        UniversityDTO updatedUniversity = universityService.updateUniversity(universityDTO);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{universityUuid}")
                .buildAndExpand(updatedUniversity.getUuid())
                .toUri();

        return ResponseEntity
                .status(HttpStatus.OK)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(updatedUniversity);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<UniversityDTO> delete(@PathVariable String uuid) {
        return ResponseEntity.ok().body(universityService.deleteUniversity(uuid));
    }

    @GetMapping("/{universityUuid}/faculties")
    public ResponseEntity<List<FacultyDTO>> getFaculties(@PathVariable String universityUuid) {
       return ResponseEntity.ok().body(universityService.getFaculties(universityUuid));
    }
}
