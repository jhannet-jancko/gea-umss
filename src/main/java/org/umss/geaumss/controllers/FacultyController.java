package org.umss.geaumss.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.umss.geaumss.dtos.FacultyDTO;
import org.umss.geaumss.services.FacultyService;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "/faculties")
public class FacultyController {

    @Autowired
    private FacultyService facultyService;

    @GetMapping
    public ResponseEntity<List<FacultyDTO>> getAll() {
        return ResponseEntity.ok().body(facultyService.getAllFaculties());
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<FacultyDTO> get(@PathVariable String uuid) {
        return ResponseEntity.ok().body(facultyService.getFaculty(uuid));
    }

    @PostMapping
    public ResponseEntity<FacultyDTO> create(@RequestBody FacultyDTO facultyDTO) {
        FacultyDTO createdFacultyDTO = facultyService.saveFaculty(facultyDTO);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{facultyUuid}")
                .buildAndExpand(createdFacultyDTO.getUuid())
                .toUri();

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(createdFacultyDTO);
    }

    @PutMapping("/{uuid}")
    public ResponseEntity<FacultyDTO> update(@PathVariable String uuid, @RequestBody FacultyDTO facultyDTO) {
        FacultyDTO updatedFacultyDTO = facultyService.updateFaculty(facultyDTO);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{facultyUuid}")
                .buildAndExpand(updatedFacultyDTO.getUuid())
                .toUri();

        return ResponseEntity
                .status(HttpStatus.OK)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(updatedFacultyDTO);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<FacultyDTO> delete(@PathVariable String uuid) {
        return ResponseEntity.ok().body(facultyService.deleteFaculty(uuid));
    }
}
