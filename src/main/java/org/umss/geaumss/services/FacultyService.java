package org.umss.geaumss.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.umss.geaumss.dtos.FacultyDTO;
import org.umss.geaumss.dtos.FacultyMapper;
import org.umss.geaumss.dtos.UniversityDTO;
import org.umss.geaumss.exceptions.NotFoundException;
import org.umss.geaumss.models.Faculty;
import org.umss.geaumss.models.University;
import org.umss.geaumss.repositories.FacultyRepository;
import org.umss.geaumss.repositories.UniversityRepository;

import java.util.List;
import java.util.Optional;

@Component
public class FacultyService {

    @Autowired
    private FacultyRepository facultyRepository;
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    private FacultyMapper facultyMapper;

    public FacultyDTO getFaculty(String facultyUuid) {
        Faculty faculty = facultyRepository.findOneByUuid(facultyUuid);
        if (faculty == null) {
            throw new NotFoundException("Faculty", facultyUuid);
        }

        return facultyMapper.toDTO(faculty, true);
    }

    public FacultyDTO saveFaculty(FacultyDTO facultyDTO) {
        UniversityDTO universityDTO = facultyDTO.getUniversity();
        if (universityDTO == null) {
            throw new NotFoundException("Faculty", null);
        }

        University university = universityRepository.findOneByUuid(universityDTO.getUuid());

        if (university == null) {
            throw new NotFoundException("Faculty", universityDTO.getUuid());
        }

        Faculty faculty = facultyMapper.getFaculty(facultyDTO, university);
        facultyRepository.save(faculty);

        return facultyMapper.toDTO(faculty, true);
    }

    public List<FacultyDTO> getAllFaculties() {
        return facultyRepository.findAll()
                .stream()
                .map(faculty -> facultyMapper.toDTO(faculty, true))
                .toList();
    }

    public FacultyDTO updateFaculty(FacultyDTO facultyDTO) {
        Faculty facultyExample = new Faculty(facultyDTO.getUuid());
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultyExample));

        if (optionalFaculty.isEmpty()) {
            throw new NotFoundException("Faculty", facultyDTO.getUuid());
        }

        UniversityDTO universityDTO = facultyDTO.getUniversity();
        University university = universityRepository.findOneByUuid(universityDTO.getUuid());

        if (university == null) {
            throw new NotFoundException("Faculty", universityDTO.getUuid());
        }

        Faculty faculty = optionalFaculty.get();

        faculty.setCode(facultyDTO.getCode());
        faculty.setName(facultyDTO.getName());
        faculty.setDeanName(facultyDTO.getDeanName());
        faculty.setAddress(facultyDTO.getAddress());
        faculty.setMission(facultyDTO.getMission());
        faculty.setVision(facultyDTO.getVision());
        faculty.setTelephone(facultyDTO.getTelephone());
        faculty.setEmail(facultyDTO.getEmail());
        faculty.setWebSite(facultyDTO.getWebSite());
        faculty.setUniversity(university);

        return facultyMapper.toDTO(facultyRepository.save(faculty), true);
    }

    public FacultyDTO deleteFaculty(String uuid) {
        Faculty facultyExample = new Faculty(uuid);
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultyExample));

        if (optionalFaculty.isEmpty()) {
            throw new NotFoundException("Faculty", uuid);
        }

        Faculty faculty = optionalFaculty.get();
        facultyRepository.delete(faculty);

        return facultyMapper.toDTO(faculty, true);
    }
}
