package org.umss.geaumss.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.umss.geaumss.dtos.FacultyDTO;
import org.umss.geaumss.dtos.FacultyMapper;
import org.umss.geaumss.dtos.UniversityDTO;
import org.umss.geaumss.dtos.UniversityMapper;
import org.umss.geaumss.exceptions.NotFoundException;
import org.umss.geaumss.models.Faculty;
import org.umss.geaumss.models.University;
import org.umss.geaumss.repositories.FacultyRepository;
import org.umss.geaumss.repositories.UniversityRepository;

import java.util.List;
import java.util.Optional;

@Component
public class UniversityService {
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private UniversityMapper universityMapper;
    @Autowired
    private FacultyMapper facultyMapper;

    public List<UniversityDTO> getAllUniversities() {
        return universityRepository.findAll()
                .stream()
                .map(university -> universityMapper.toDTO(university))
                .toList();
    }

    public UniversityDTO getUniversity(String universityUuid) {
        University universityExample = new University(universityUuid);
        Optional<University> optionalUniversity = universityRepository.findOne(Example.of(universityExample));

        if (optionalUniversity.isEmpty()) {
            throw new NotFoundException("University", universityUuid);
        }

        University university = optionalUniversity.get();

        return universityMapper.toDTO(university);
    }

    public UniversityDTO save(UniversityDTO universityDTO) {
        University university = universityMapper.toUniversity(universityDTO);
        return universityMapper.toDTO(universityRepository.save(university));
    }

    public List<FacultyDTO> getFaculties(String universityUuid) {
        University universityExample = new University();
        universityExample.setUuid(universityUuid);
        Optional<University> optionalUniversity = universityRepository.findOne(Example.of(universityExample));

        if (optionalUniversity.isEmpty()) {
            throw new NotFoundException("university", universityUuid);
        }

        University university = optionalUniversity.get();

        Faculty facultyExample = new Faculty();
        University universityFilterExample = new University();
        universityExample.setId(university.getId());
        facultyExample.setUniversity(universityFilterExample);

        //review issue
        List<Faculty> faculties = facultyRepository.findAll(Example.of(facultyExample));
        return faculties
                .stream()
                .map(faculty -> facultyMapper.toDTO(faculty, false))
                .toList();
    }

    public UniversityDTO updateUniversity(UniversityDTO universityDTO) {
        University universityExample = new University(universityDTO.getUuid());
        Optional<University> optionalUniversity = universityRepository.findOne(Example.of(universityExample));

        if (optionalUniversity.isEmpty()) {
            throw new NotFoundException("University", universityDTO.getUuid());
        }

        University university = optionalUniversity.get();

        university.setCode(universityDTO.getCode());
        university.setName(universityDTO.getName());

        return universityMapper.toDTO(universityRepository.save(university));
    }

    public UniversityDTO deleteUniversity(String uuid) {
        University universityExample = new University(uuid);
        Optional<University> optionalUniversity = universityRepository.findOne(Example.of(universityExample));

        if (optionalUniversity.isEmpty()) {
            throw new NotFoundException("University", uuid);
        }

        University university = optionalUniversity.get();
        universityRepository.delete(university);

        return universityMapper.toDTO(university);
    }
}
