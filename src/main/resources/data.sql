insert into university(code, name, uuid, id, deleted)
values ('UMSS', 'Universidad Mayor de San Simon', 'aff6e021-90d9-45fe-bc0c-a8692e091620', 1000, false);
insert into university(code, name, uuid, id, deleted)
values ('UCB', 'Universidad Catolica Boliviana', 'aff6e021-90d9-45fe-bc0c-a8692e091621', 1001, false);
insert into university(code, name, uuid, id, deleted)
values ('Univalle', 'Universidad Privada del Valle', 'aff6e021-90d9-45fe-bc0c-a8692e091622', 1002, false);

insert into faculty (code, name, university_id, uuid, id)
values ('FCYT',  'Facultad de ciencias y tecnologia', 1000, 'aff6e021-90d9-45fe-bc0c-a8692e091623', 2000);
insert into faculty (code,  name, university_id, uuid, id)
values ('FCE', 'Facultad de Ciencias Economicos', 1000, 'aff6e021-90d9-45fe-bc0c-a8692e091624', 2001);
insert into faculty (code, name, university_id, uuid, id)
values ('FCJ', 'Facultad de ciencias juridicas', 1001, 'aff6e021-90d9-45fe-bc0c-a8692e091625', 2002);
